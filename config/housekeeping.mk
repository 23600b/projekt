ifdef debug
ifeq ($(debug),True)
.SECONDARY:
endif
endif

_:=$(shell mkdir --parent "logs" 2>/dev/null)
_:=$(shell mkdir --parent "temp" 2>/dev/null)
_:=$(shell mkdir --parent "data/analyses" 2>/dev/null)
