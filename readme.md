PROJEKT
=======

A simple backbone to a data analysis project using `GNU make`.  
`blocks/test` is there only for demostration purposes; in fact, it is listed in
`.gitignore`, which means after forking you probably want to either remove it or
just reuse it for your own tests.

PROJEKT assumes that your version of `make` supports `export`.  
I have no idea since which version `export` is supported.
