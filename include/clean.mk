clean:
	rm -rf "logs"
	rm -rf "temp"
	@tools/make/choice "Remove analyses"
	rm -rf "data/analyses"

tidy:
	rm -rf "temp"
